/-------------------------------------------------
| Welcome to the Debian derivatives census scripts!

The Debian derivatives census scripts aim to take information added to the
Debian derivatives census wiki pages and aggregate, expand, check, massage and
transform it in order to integrate information useful to Debian contributors
into the Debian infrastructure. More information about Debian derivatives and
the derivatives census is available on these wiki pages:

https://wiki.debian.org/DerivativesFrontDesk
https://wiki.debian.org/Derivatives/Census
https://wiki.debian.org/Derivatives/Integration
https://wiki.debian.org/Derivatives/CensusQA
https://wiki.debian.org/Derivatives/CensusFull

The scripts are run via a framework based on make. With the make-based
framework, data from each derivative is stored in a subdirectory of the
var directory. The scripts are named in self-explanatory ways, take the
names of input and output as command-line arguments and contain comments
indicating their purpose and if non-obvious, how they work.

/-------------------------------------------------
| Make machinery

Makefile.{var,deriv} are symlinked from the var/ dir and from each of the
directories for the derivatives. They setup environment variables, declare
files to be generated, their dependencies and how to create them.

Makefile: the main makefile machinery, used to setup var dir
Makefile.var: used to aggregate files and setup derivative dirs
Makefile.deriv: used by each derivative dir to create files

/-------------------------------------------------
| Tips for using make

To force re-making a file:

  $ make -B filename

To force re-making a file but force not updating its prerequisites:

  $ make -o prerequisitename -B filename

These are very useful when developing or fixing census scripts.

/-------------------------------------------------
| Scripts

All the scripts are stored in the bin/ directory and run by the make-based
framework.

These download information from the Internet:

get-url
get-url-eol-fix
get-blogs-rss
get-distrowatch-stats
get-favicon
get-favicon-img
get-logo-img
get-package-lists
get-reprepro-config
get-wiki-html
get-wiki-text

These extract information from census wiki pages:

wiki-html-to-derivatives-list
wiki-text-to-blogs-list
wiki-text-to-dev-blogs-list
wiki-text-to-distrowatch
wiki-text-to-homepage
wiki-text-to-logo
wiki-text-to-name
wiki-text-to-sources-list
wiki-text-to-status
wiki-text-to-vendor

These convert data files to be suitable for Debian infrastructure:

logo-to-planet-head

These create configuration snippets for Debian infrastructure:

derivative-to-planet-config
derivative-to-planet-head

These aggregate configuration for Debian infrastructure:

aggregate-planet-config
aggregate-planet-heads

These aggregate the derivatives' source files and patches:

aggregate-sources-files
aggregate-sources-patches

These form dd-lists of patched packages:

sources-patches-to-packages
packages-to-dd-list

These check the derivatives for potential issues:

check-package-list
check-sources-list

These compare the derivatives with Debian:

compare-planet-config
compare-planet-heads
compare-source-package-list
compare-images

These are hacks to deal with the apt OpenPGP key stuff:

fakegpgv
gpgv

These are run by the etc/*.crontab configuration files for the cron daemon:

general-cron
snapshot-cron

/-------------------------------------------------
| Configuration files

These are stored in the etc/ directory.

curl.conf: sets some parameters for curl
htaccess.*: sets some parameters for Apache
architectures: list of Debian related architectures
disabled-targets: lists of Makefile targets which are not to be run

You can update the list of architectures by running
this command on the snapshot.debian.org server:

psql service=snapshot-guest -t -c "SELECT DISTINCT architecture FROM file_binpkg_mapping WHERE architecture NOT IN ('all', 'freebsd-i386') ORDER BY architecture;" | tr -d ' '

/-------------------------------------------------
| Data files

These are stored in the var/ dir as well as the directories for each
Debian derivative:

HEADER.html: a symlink to documentation for visitors via HTTP
FOOTER.html: a symlink to a footer for visitors via HTTP
.htaccess: sets some parameters for Apache
debian.apt/: a symlink to an apt/ directory for Debian experimental and unstable
sources.patches.dd-list: Maintainer/Uploaders data for the packages received from
                         Debian debian.apt/var/lib/apt/lists/*_source_Sources
sources.patches.dd-list.unknown-packages: for those the Sources files are missing

These are stored in the var/ dir:

census.html: the HTML version of the census wiki page
derivatives: a list of names of derivative wiki pages
distrowatch: distrowatch download statistics
planet-config.ini: config file for Planet Debian derivatives
planet-git/: Git repository of Planet Debian configuration
planet-heads/: small logos for Planet Debian derivatives
planet-heads.tar: tarball of Planet heads for the review script
template.txt: the text version of the census template wiki page

These are also in the var/ dir but are only created on snapshot.debian.org:

md5-farm/: symlinks mapping MD5 hashes to SHA-1 hashes
patches/: symlinks mapping human-readable patch names to SHA-1 based names
sha256-farm/: symlinks mapping SHA-256 hashes to SHA-1 hashes
sha1-farm/: SHA-1 based filesystem of files never in Debian
sha1-changelog/: (name, version) tuples of source packages never in Debian
sha1-patches/: patches between source packages based on SHA-1 hashes
sha1-lsdiff/: lsdiff output from the patches

These are stored in the derivative dirs and are extracted from wiki text:

name: long name
logo: URL to the logo
blogs: URLs to the main blogs
blogs-dev: URLs to the developer blogs
homepage: URL to the homepage
distrowatch: the identifier from distrowatch.com
sources.list: the sources.list snippets

These are stored in the derivative dirs and are downloaded from the Internet:

wiki.txt: the text versions of the derivative wiki pages
apt/: apt package data, including Release/Package/Sources files
blogs-rss: the URLs to RSS feeds for the main blogs
blogs-dev-rss: the URLs to RSS feeds for the developer blogs
logo-img: the image of the logo
reprepro.config: the derivative's reprepro config

These are stored in the derivative dirs and are problem reports:

check-sources-list: potential issues with the sources.list snippet
check-package-list: potential issues with the apt metadata
packages_binary_packages: list of binary packages in Packages files
sources_binary_packages: list of binary packages in Sources files
sources_source_packages: list of source packages in Packages files
packages_source_packages: list of source packages in Sources files
diff_source_packages: diff of sources_source_packages and packages_source_packages
diff_binary_packages: diff of sources_binary_packages and packages_binary_packages

These are also in the derivative dirs but are only created on snapshot.debian.org:

patches: symlinks mapping human-readable patch names to SHA-1 based names
sources.patches: package name/version and patches
sources.links: URLs to modified source packages
sources.files: YAML mapping between SHA-1 and other hashes of modified files
sources.stamp: implementation detail due to a deficiency in make
sources.new: source packages that were never in Debian
sources.log: log file from the compare-source-package-list script

Files named *.remote-timestamp store remote timestamps to reduce downloading.

/-------------------------------------------------
| Package dependencies

These packages are needed for all the scripts that are run by default:

coreutils | busybox
bash | dash | zsh | sh
diffutils
curl
file
sed
apt
perl
libxml-feed-perl
python3
python3-apt
python3-debian
python3-pil
python3-cairosvg

This package is only needed for the download of favicons, which are not
currently used by the census:

libwww-favicon-perl

The packages are only used when running the compare-source-package script,
which is only run on snapshot.debian.org:

python3-psycopg2
python3-urllib3
python3-requests
python3-simplejson (optional)
python3-yaml
devscripts
patchutils
diffstat
dpkg-dev
