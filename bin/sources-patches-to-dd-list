#!/bin/sh

# Copyright 2018 Anastasia Tsikoza
# Released under the MIT/Expat license, see doc/COPYING

# Converts a list of packages into the dd-list
#
# Usage:
# packages-to-dd-list <sources.patches> <debian.apt.conf> <sources.patches.dd-list>

sources_patches="$1"
export APT_CONFIG="$2"
dd_list="$3"
unknown="$dd_list.unknown-packages"
errors="$dd_list.errors"

# Retrieve Debian names of packages from the sources.patches file
# then pass packages to dd-list and handle unknown packages
# (missing in the *_source_Sources files) and other errors
# Using grep is for the extraction is much faster than parsing YAML.
grep '^- debian_name: ' "$sources_patches" 2> /dev/null | cut -d ' ' -f3 |
dd-list --stdin 2>&1 > "$dd_list" |
sed --silent --expression "/^E: Unknown package: / { s///; w $unknown" \
             --expression "; b; }; w $errors"

find "$unknown" -empty -delete
find "$errors" -empty -delete
