#!/usr/bin/python3

# Copyright 2012 Paul Wise
# Released under the MIT/Expat license, see doc/COPYING

# Creates a logo head for Planet Debian derivatives based on the logos.
#
# Usage:
# logo-to-planet-head <logo image> <planet head image>

import os
import sys

default_size = (100,100)

logo = sys.argv[1]
head = sys.argv[2]

def remove(file):
	try: os.remove(file)
	except OSError: pass

def symlink(source, link):
	try: os.symlink(source, link)
	except OSError: pass

def process_svg():
	import cairosvg
	cairosvg.svg2png(logo, write_to=head,
			parent_width=default_size[0],
			parent_height=default_size[1])

def process_image():
	from PIL import Image
	img = Image.open(logo)
	img.thumbnail(default_size, Image.ANTIALIAS)
	img.save(head, 'PNG')

try:
	if logo.endswith('.svg'):
		process_svg()
	else:
		process_image()
except (ImportError, IOError):
	remove(head)
	if os.path.exists(logo):
		symlink(logo, head)
